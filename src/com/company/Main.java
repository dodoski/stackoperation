package com.company;


public class Main {

    public static void main(String[] args) {
	// write your code here
        StackImplementation stack = new StackImplementation();

        stack.push("Jestem");
        stack.push("Dobrym");
        stack.push("Czlowiekiem");
        stack.push("Szukajacym");
        stack.push("Ludzi");
        stack.push("Gotowych");
        stack.push("Pojsc");
        stack.push("Za");
        stack.push("Nim");

        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
        System.out.println(stack.pop());
        System.out.println(stack.get());
    }
}
