package com.company;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StackImplementation implements StackOperations {
    final private int STACK_HEAD = 0;
    List<String> stack = new ArrayList();


    @Override
    public void push(final String item){
        stack.add(STACK_HEAD, item);
    }

    @Override
    public Optional<String> pop(){
        if(stack.isEmpty()){
            return Optional.empty();
            }
        else{
            return Optional.of(stack.remove(STACK_HEAD));
            }
    }

    @Override
    public List<String> get(){
        return stack;
    }



}
